<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\TaskRequest;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function index()
    {
        return response()->json(['tasks' => Task::all()],200);
    }

    public function store(TaskRequest $request)
    {
        try{
            $task = Task::create($request->all());
            return response()->json(['task' => $task,'message' => 'Successfully added task'],201);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()],$exception->getCode());
        }
    }

    public function show($id)
    {
        $task = Task::find($id);
        return response()->json(['task' => $task],200);
    }

    public function update(TaskRequest $request, $id)
    {
        try{
            $task = Task::find($id);
            $task->update($request->all());
            return response()->json(['task' => $task,'message' => 'Successfully updated task'],200);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()],$exception->getCode());
        }
    }

    public function destroy($id)
    {
        try{
            $task = Task::find($id);
            $task->delete();
            return response()->json(['task' => $task,'message' => 'Successfully deleted task'],200);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()],$exception->getCode());
        }
    }
}
